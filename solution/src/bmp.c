#include "bmp.h"
#include <inttypes.h>

uint64_t padding(const struct image * image){
    return image->width % 4;
}


struct bmp_header new_header(const struct image* image){
    struct bmp_header header = {
            .bfType = BF_TYPE,
            .bfileSize = image->height * image->width * sizeof(struct pixel) +
                         image->height * padding(image) * sizeof(struct pixel),
            .bfReserved = BF_RESERVED,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BI_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = BI_PLANES,
            .biBitCount = BIT_COUNT,
            .biCompression = BI_COMPRESSION,
            .biSizeImage = image->height * image->width * sizeof(struct pixel) + padding(image) * image->height,
            .biXPelsPerMeter = BI_X_PELS_PER_METER,
            .biYPelsPerMeter = BI_Y_PELS_PER_METER,
            .biClrUsed = BI_CLR_USED,
            .biClrImportant = BI_CLR_IMPORTANT
    };
    return header;
}


enum read_status from_bmp(struct image * image, FILE * file){
    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1,  file) != 1) {
        return READ_INVALID_HEADER;
    }
    image = create_image(image, header.biHeight, header.biWidth);
    for (size_t i = 0; i < image->height; i++) {
        if(!fread(&(image->data[i * image->width]), sizeof(struct pixel), image->width, file))return READ_INVALID_BITS;
        if(fseek(file, (uint8_t) padding(image), SEEK_CUR)!=0)return READ_INVALID_SIGNATURE;
    }
    return READ_OK;
}

enum write_status to_bmp(struct image * image, FILE * file){
    struct bmp_header header = new_header(image);
    if (fwrite(&header, sizeof(struct bmp_header), 1, file) != 1) {
        return WRITE_ERROR;
    }
    const size_t fill = 0;
    for (size_t i = 0; i < image->height; i++) {
        if(!fwrite(&(image->data[i * image->width]), sizeof(struct pixel), image->width, file))return WRITE_ERROR;
        if(!fwrite(&fill, 1, image->width % 4, file)) return WRITE_ERROR;
    }
    return WRITE_OK;
}

