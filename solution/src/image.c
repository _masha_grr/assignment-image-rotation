
#include "bmp.h"


struct image * create_image_malloc(struct image * image){
    image = malloc(sizeof(struct image));
    return image;
}

struct image * create_image(struct image * image, uint64_t height, uint64_t width){
    image->width = width;
    image->height = height;
    image->data = malloc(image->height * image->width * sizeof(struct pixel));
    return image;
}

void image_destruct(struct image * image){
    if (image->data){
        free(image->data);
    }
    free(image);
}

void get_info(struct image * image){
    printf("Image info:\n");
    printf("Size: ");
    printf("%"PRIu64, (image->height * image->width * sizeof(struct pixel) +
                       image->height * padding(image) * sizeof(struct pixel)));
    printf(" bytes \nWidth: ");
    printf("%"PRIu64, image->width);
    printf("\nHeight: ");
    printf("%"PRIu64, image->height);
    printf("\n");

}
