#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

//#include "io.h"
#include "bmp.h"
//#include "rotation.h"
//#include "image.h"
//#include <unistd.h>
//#include <string.h>
//#define RED "\e[0;31m"

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    printf("=======================================\n");
    if(argc != 3){
        printf("[ERROR]: number of arguments are invalid\n");
        return 1;
    }
    FILE * file_for_read = {0};
    FILE * file_for_write = {0};
    if((open_for_read(&file_for_read, argv[1]))!=READ_OK){
        printf("[ERROR]: no such file or directory 1st arg");
        return 1;
    }
    if(open_for_write(&file_for_write, argv[2])!=READ_OK){
        printf("[ERROR]: no such file or directory 2nd arg");
        return 1;
    }

    struct image * initial_image = NULL;
    initial_image = create_image_malloc(initial_image);
    if(from_bmp(initial_image, file_for_read) != READ_OK){
        printf("[ERROR]: unable to read file");
        return 1;
    }

    get_info(initial_image);
    struct image * transformed_image = NULL;
    transformed_image = transform(initial_image);

    if(to_bmp(transformed_image, file_for_write) != WRITE_OK){
        printf("[ERROR]: unable to write file");
        return 1;
    }
    image_destruct(initial_image);
    image_destruct(transformed_image);


    printf("=======================================\n");
    if(!close_f(file_for_read)){
        printf("UNABLE TO CLOSE INITIAL FILE");
    }
    if(!close_f(file_for_write)){
        printf("UNABLE TO CLOSE NEW FILE");
    }

    return 0;
}
