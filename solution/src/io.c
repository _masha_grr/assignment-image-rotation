#include "io.h"

enum read_status open_for_read(FILE ** f, const char * argv){
    *f = fopen(argv, "rb");
    if (*f==NULL) return READ_INVALID_BITS;
    return READ_OK;
}
enum read_status open_for_write(FILE ** f, const char * argv){
    *f = fopen(argv, "wb");
    if (*f==NULL) return READ_INVALID_BITS;
    return READ_OK;
}
bool close_f(FILE * file){
    if(EOF != fclose(file)) return true;
    return false;
}
