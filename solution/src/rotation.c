#include "rotation.h"

struct pixel get_pixel(const struct image * image, size_t x, size_t y) {
    const size_t pos = y * image->width + x;
    return image->data[pos];
}

struct pixel * get_pixel_ptr(struct image * image, size_t x, size_t y) {
    const size_t pos = y * image->width + x;
    return image->data + pos;
}


struct image *transform(struct image * old_image) {
    if(!old_image) return NULL;
    struct image * new_image = NULL;
    new_image = create_image_malloc(new_image);
    new_image = create_image(new_image, old_image->width, old_image->height);
    for (size_t x = 0; x < old_image->width; x++) {
        for (size_t y = 0; y < old_image->height; y++) {
            *get_pixel_ptr(new_image, y, x) = get_pixel(old_image, x, new_image->width - y-1);
        }
    }
    return new_image;
}
