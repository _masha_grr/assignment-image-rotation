#include "io.h"
#include "header.h"


uint64_t padding(const struct image * image);
enum read_status from_bmp(struct image * image, FILE * file);
enum write_status to_bmp(struct image * image, FILE * file);
