#include <stdio.h>
#include <stdbool.h>
#include "rotation.h"

enum read_status open_for_read(FILE ** f, const char * argv);
enum read_status open_for_write(FILE ** f, const char * argv);
bool close_f(FILE * file);
