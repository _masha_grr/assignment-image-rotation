#include  <stdint.h>

#pragma once
static const uint16_t BF_TYPE = 0x4D42;
static const uint32_t BF_RESERVED = 0;
static const uint32_t BI_SIZE = 40;
static const uint16_t BI_PLANES = 0;
static const uint16_t BIT_COUNT = 24;
static const uint32_t BI_COMPRESSION = 0;
static const uint32_t BI_X_PELS_PER_METER = 0;
static const uint32_t BI_Y_PELS_PER_METER = 0;
static const uint32_t BI_CLR_USED = 0;
static const uint32_t BI_CLR_IMPORTANT = 0;


struct  __attribute__((packed))bmp_header{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
