#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

struct __attribute__((packed)) pixel { uint8_t b, g, r; };
struct image {
    uint64_t width, height;
    struct pixel* data;
};

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};

void image_destruct(struct image * image);
struct image * create_image_malloc(struct image * image);
struct image * create_image(struct image * image, uint64_t height, uint64_t width);
void get_info(struct image * image);
